[Atenção]
Projeto deixou de ser atualizado no GitLab em 05/08/2022 .

Para clonar o projeto mais atualizado, utilize o link https://github.com/diegofelisbino/POKE-API-BFF

# POKE-API-BFF [Em desenvolvimento]
Uma API desenvolvida em .NET para servir como um BFF para dois clientes distintos, com necessidades especificas.

Tecnologias utilizadas no projeto:

- AutoMapper
- Refit

Tests:
  - xUnit
    - Testes Unitários 
    - Testes de Integração
    - Testes de Mutação
      - Stryker.Net
  
  
